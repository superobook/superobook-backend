import {BasicRest} from "../BasicRest";
import {OpenHandler} from "../../handlers/model/OpenHandler";
import Handler from "../../handlers/model/OpenHandler";
import * as HTTPStatus from 'http-status-codes';

export class Open extends BasicRest {
  protected _handler: OpenHandler;

  constructor (router) {
    super(router, Handler);

    this.routes = {
      get: {
        '/books/amount': this.readBooksAmount.bind(this),
        '/books/:page/:amountPerPage': this.readBooks.bind(this),
        '/book/:bookId/': this.readBook.bind(this),
      },
    };

    this.wiring();
  }

  set handler (value: OpenHandler) {
    this._handler = value;
  }

  get handler (): OpenHandler {
    return this._handler;
  }

  set routes (rotas) {
    this._routes = rotas;
  }

  get routes () {
    return this._routes;
  }

  private async readBooksAmount (req, res) {
    let response = await this.handler.readBooksAmount({
      filter: req.query.filter,
      initialYear: req.query.initialYear,
      endYear: req.query.endYear,
    });
    res
      .status(HTTPStatus.OK)
      .send(response);
  }

  private async readBooks (req, res) {
    let response = await this.handler.readBooks({
      filter: req.query.filter,
      initialYear: req.query.initialYear,
      endYear: req.query.endYear,
      page: req.params.page,
      amountPerPage: req.params.amountPerPage,
    });
    res
      .status(HTTPStatus.OK)
      .send(response);
  }

  private async readBook (req, res) {
    let response = await this.handler.readBook({
      id: req.params.bookId,
    });
    res
      .status(HTTPStatus.OK)
      .send(response);
  }

}