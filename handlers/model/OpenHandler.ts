import {BasicHandler} from "../BasicHandler";
import {FindObject} from '../util/FindObject';

export class OpenHandler extends BasicHandler {

  public async readBooksAmount (param: filter): Promise<basicReturn> {
    try {
      const
        booksAmount = await this.sendToServer(`db.book.count`, new FindObject({
          query: OpenHandler.mountQuery(param),
        }));
      return await this.returnHandler({
        model: 'book',
        data: booksAmount.data,
      });
    } catch (error) {
      return await this.returnHandler({
        model: 'book',
        data: error.message || error
      });
    }
  }

  public async readBooks (param: readBooks): Promise<basicReturn> {
    let required = this.attributeValidator([
      "page", "amountPerPage"
    ], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      const query = OpenHandler.mountQuery(param);
      const
        books = await this.sendToServer(`db.book.read`, new FindObject({
          query,
          select: `title author publishingCompany year id _id`,
          pagination: {
            limit: Number(param.amountPerPage),
            nextPage: Number(param.page),
          }
        }));
      return await this.returnHandler({
        model: 'book',
        data: books.data,
      });
    } catch (error) {
      return await this.returnHandler({
        model: 'book',
        data: error.message || error
      });
    }
  }
  
  public async readBook (param: {id: string}): Promise<basicReturn> {
    let required = this.attributeValidator([
      "id"
    ], param);
    if(!required.success) return await this.getErrorAttributeRequired(required.error);
    try {
      const
        books = await this.sendToServer(`db.book.read`, new FindObject({
          query: param.id,
          select: `title author publishingCompany year id _id language weight dimension.length dimension.width dimension.height`,
        }));
      return await this.returnHandler({
        model: 'book',
        data: books.data,
      });
    } catch (error) {
      return await this.returnHandler({
        model: 'book',
        data: error.message || error
      });
    }
  }

  private static mountQuery (param: filter) {
    let
      query = {},
      queryOr = null,
      queryYear = null;
    if(param.filter) {
      queryOr = {
        $or: [
          {
            title: {
              $regex: `.*${param.filter}.*`,
              $options: 'si',
            },
          },
          {
            author: {
              $regex: `.*${param.filter}.*`,
              $options: 'si',
            },
          },
          {
            ISBN: param.filter,
          }
        ]
      };
    }
    if(param.initialYear || param.endYear) {
      if(param.initialYear && param.endYear) queryYear = {
        $and: [
          {
            year: {$gte: Number(param.initialYear)}
          },
          {
            year: {$lte: Number(param.endYear)}
          }
        ]
      };
      else if(param.initialYear) queryYear = {year: {$gte: Number(param.initialYear)}};
      else queryYear = {year: {$lte: Number(param.endYear)}};
    }
    if(queryYear && queryOr) return {$and: [queryYear, queryOr]};
    else if(queryOr) return queryOr;
    else if(queryYear) return queryYear;
    else return query;
  }

}

export default new OpenHandler();

interface basicReturn {
  success: boolean,
  data: any
}

interface filter {
  filter?: string
  initialYear?: string,
  endYear?: string,
}

interface readBooks extends filter {
  page: number,
  amountPerPage: number,
}