import * as path from 'path';
import {TestManager} from '../TestManager';

const chai: any = require('chai');
const chaiHTTP = require('chai-http');
const config = require(path.resolve('devConfig.json'));

chai.use(chaiHTTP);
let expect = chai.expect;
let testManager = null;
const baseURL = `http://localhost:${config.server.port}`;

describe('1.OpenAPI', () => {

  before((done) => {
    testManager = new TestManager(done);
  });

  describe('Books amount', () => {

    it('will read amount by year', (done) => {
      chai.request(baseURL)
        .get("/api/books/amount?initialYear=2016&endYear=2019")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          done();
        });
    });

    it('will read amount without filter', (done) => {
      chai.request(baseURL)
        .get("/api/books/amount")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          expect(response.body.data).to.be.equal(100);
          done();
        });
    });

    it('will read amount by title/author', (done) => {
      chai.request(baseURL)
        .get("/api/books/amount?filter=of")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          done();
        });
    });

    it('will read amount by title/author and year', (done) => {
      chai.request(baseURL)
        .get("/api/books/amount?filter=of&initialYear=2000&endYear=2014")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          done();
        });
    });

    it('will read amount by ISBN', (done) => {
      chai.request(baseURL)
        .get("/api/books/amount?filter=80")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          done();
        });
    });

  });

  describe('Read books', () => {

    it('will read books by page and year', (done) => {
      chai.request(baseURL)
        .get("/api/books/1/20/?initialYear=2017&endYear=2019")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          expect(response.body.data).to.be.instanceOf(Array);
          response.body.data.forEach(book => {
            expect(book).to.be.instanceOf(Object);
            expect(book).to.have.all.keys("title", "author", "publishingCompany", "year");
          });
          done();
        });
    });

    it('will read books by page and filter', (done) => {
      chai.request(baseURL)
        .get("/api/books/1/20/?filter=of")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          expect(response.body.data).to.be.instanceOf(Array);
          response.body.data.forEach(book => {
            expect(book).to.be.instanceOf(Object);
            expect(book).to.have.all.keys("title", "author", "publishingCompany", "year");
          });
          done();
        });
    });

    it('will read books by page, filter and year', (done) => {
      chai.request(baseURL)
        .get("/api/books/1/20/?filter=of&initialYear=2000&endYear=2014")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          expect(response.body.data).to.be.instanceOf(Array);
          response.body.data.forEach(book => {
            expect(book).to.be.instanceOf(Object);
            expect(book).to.have.all.keys("title", "author", "publishingCompany", "year");
          });
          done();
        });
    });

    it('will read all books by page', (done) => {
      chai.request(baseURL)
        .get("/api/books/2/20")
        .end((error, response) => {
          expect(response.body).to.be.instanceof(Object);
          expect(response.body).to.have.all.keys("success", "data");
          expect(response.body.success).to.be.true;
          expect(response.body.data).to.be.instanceOf(Array);
          response.body.data.forEach(book => {
            expect(book).to.be.instanceOf(Object);
            expect(book).to.have.all.keys("title", "author", "publishingCompany", "year");
          });
          done();
        });
    });

  });

});