import {BasicManager} from "../BasicManager";
import {Model} from "../model/Book";

export class Book extends BasicManager {
  wireCustomListeners () {}

  get model () {
    return Model;
  }

  get eventName () {
    return 'book';
  }
}