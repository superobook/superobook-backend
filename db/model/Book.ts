import {model, Schema} from "mongoose";
import {BaseSchema} from "../BaseSchema";
import {Dimension} from '../subSchema'

let schema_options = {
  timestamps: true,
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  },
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
};

let schema = new Schema({
  ...{
    title: {
      type: Schema.Types.String,
      trim: true,
      required: [true, `titleRequired`]
    },
    ISBN: {
      type: Schema.Types.String,
      trim: true,
      required: [true, 'ISBNRequired'],
    },
    author: {
      type: Schema.Types.String,
      trim: true,
      required: [true, 'authorRequired'],
    },
    publishingCompany: {
      type: Schema.Types.String,
      trim: true,
      required: [true, 'publishingCompanyRequired'],
    },
    year: {
      type: Schema.Types.Number,
      required: [true, `yearRequired`]
    },
    language: {
      type: Schema.Types.String,
      trim: true,
      required: [true, 'publishingCompanyRequired'],
    },
    weight: {
      type: Schema.Types.Number,
      required: [true, `weightRequired`]
    },
    dimension: {
      type: Dimension,
      required: [true, `dimensionRequired`]
    }
  },
  ...BaseSchema
}, schema_options);

const Model = model("book", schema);
export {Model};