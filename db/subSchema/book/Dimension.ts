import {Schema} from "mongoose";

let schema_options = {
  toJSON: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      return ret;
    }
  },
  toObject: {
    virtuals: true,
    transform: function (doc, ret) {
      delete ret._id;
      return ret;
    }
  }
};

let schema = new Schema({
  length: {
    type: Schema.Types.Number,
    required: [true, 'lengthRequired'],
  },
  width: {
    type: Schema.Types.Number,
    required: [true, 'widthRequired'],
  },
  height: {
    type: Schema.Types.Number,
    required: [true, 'heightRequired'],
  },
}, schema_options);

export {schema};
