import {ManagerMap} from "../interfaces/ManagerMap";
import {Book} from './managers/Book'

/**
 * Inicia todos os managers.
 */
let Managers: ManagerMap = {
  book: new Book(),
};

export {Managers};